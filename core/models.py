from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from model_utils.models import TimeStampedModel
from .helpers import random_string_generator


class AppUser(AbstractUser):
    unique_code = models.CharField(max_length=255, unique=True)
    user_type = models.CharField(max_length=20, choices=settings.USER_TYPE)
    mobile_number = models.CharField(max_length=15, null=True, blank=True)
    relationships = models.ManyToManyField('self', through='Relationship',
                                           symmetrical=False,
                                           related_name='related_to+')
    verified_email = models.BooleanField(default=False)
    balance = models.BigIntegerField(default=0)

    REQUIRED_FIELDS = ['email']

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.unique_code:
            self.unique_code = random_string_generator(size=200)
            self.verified_email = False
        super(AppUser, self).save()

    def activate(self):
        self.verified_email = True
        self.save()

    def add_relationship(self, person, status=1, symm=True):
        relationship, created = Relationship.objects.get_or_create(
            from_person=self,
            to_person=person,
            status=status)
        if symm:
            # avoid recursion by passing `symm=False`
            person.add_relationship(self, status, False)
        return relationship

    def remove_relationship(self, person, status=1, symm=True):
        Relationship.objects.filter(
            from_person=self,
            to_person=person,
            status=status).delete()
        if symm:
            # avoid recursion by passing `symm=False`
            person.remove_relationship(self, status, False)

    def get_relationships(self, status=1):
        return self.relationships.filter(
            to_people__status=status,
            to_people__from_person=self)


class Relationship(models.Model):
    from_person = models.ForeignKey(AppUser, related_name='from_people')
    to_person = models.ForeignKey(AppUser, related_name='to_people')
    status = models.IntegerField(choices=settings.RELATIONSHIP_STATUSES)


class CardCategory(TimeStampedModel):
    name = models.CharField(max_length=1000, unique=True)
    description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Card Categories'


class Card(TimeStampedModel):
    category = models.ForeignKey(CardCategory, related_name='cards')
    creator = models.ForeignKey(AppUser, related_name='created_cards')
    receiver = models.ForeignKey(AppUser, related_name='received_cards')
    description = models.TextField(null=True, blank=True)
    activated_on = models.DateTimeField(null=True, blank=True)
    amount = models.BigIntegerField(default=0)
    unique_code = models.CharField(max_length=255, unique=True)
    link_clicked = models.BooleanField(default=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.unique_code:
            self.unique_code = random_string_generator(size=200)
            self.verified_email = False
        super(Card, self).save()

    def __unicode__(self):
        return "%s - %s - %s" % (self.category, self.receiver, self.amount)


class WebhookListener(TimeStampedModel):
    webhook_id = models.CharField(max_length=1024, null=True, blank=True)
    account_id = models.CharField(max_length=1024, null=True, blank=True)
    receipt = models.TextField(null=True, blank=True)
    vendor = models.TextField(null=True, blank=True)
    order_id = models.TextField(null=True, blank=True)
    person_info = models.TextField(null=True, blank=True)
    total_value = models.TextField(null=True, blank=True)
    rest_of_the_data = models.TextField()
    sent_by = models.TextField(null=True, blank=True)
    sender_email = models.TextField(null=True, blank=True)

    def __unicode__(self):
        if self.sent_by and self.sender_email:
            return "%s - %s" % (self.sent_by, self.sender_email)
        else:
            return "Webhook Listener Object"
