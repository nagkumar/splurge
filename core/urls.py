__author__ = 'nagkumar'
from django.conf.urls import patterns, include, url


urlpatterns = patterns('core.views',
                       url(r'^sign_up/$', 'sign_up', name='sign_up'),
                       url(r'^login/$', 'custom_login', name='custom_login'),
                       url(r'^logout/$', 'custom_log_out', name='custom_log_out'),
                       url(r'^home/$', 'home', name='home'),
                       url(r'^create_card/$', 'create_card', name='create_card'),
                       url(r'^list_cards/$', 'list_cards', name='list_cards'),
                       url(r'^get_info/(?P<id>.+)/$', 'get_info', name='get_info'),
                       url(r'^activate/(?P<code>.+)/$', 'activate_user', name='activate_user'),
                       url(r'^activate_card/(?P<code>.+)/$', 'activate_card', name='activate_card'),
                       url(r'^new_mail_received/', 'new_mail_received', name='new_mail_received'),
                       url(r'^', 'welcome_page', name='welcome_page'),

)