from crispy_forms.bootstrap import StrictButton
from crispy_forms.layout import Submit, Layout

__author__ = 'nagkumar'

from .models import AppUser
from django import forms
from crispy_forms.helper import FormHelper


class AppUserForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AppUserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit('submit', 'Submit'))

    class Meta:
        model = AppUser
        exclude = ("is_staff", "is_active", "date_joined", "last_login", "user_permissions", "groups", "relationships",
                   "user_type", "unique_code", "verified_email")