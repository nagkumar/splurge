# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_webhooklistener_total_value'),
    ]

    operations = [
        migrations.AddField(
            model_name='webhooklistener',
            name='sender_email',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='webhooklistener',
            name='sent_by',
            field=models.TextField(null=True, blank=True),
        ),
    ]
