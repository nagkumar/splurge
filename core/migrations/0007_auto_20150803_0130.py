# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20150725_0043'),
    ]

    operations = [
        migrations.AddField(
            model_name='webhooklistener',
            name='order_id',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='webhooklistener',
            name='person_info',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='webhooklistener',
            name='receipt',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='webhooklistener',
            name='vendor',
            field=models.TextField(null=True, blank=True),
        ),
    ]
