# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20150803_0130'),
    ]

    operations = [
        migrations.AddField(
            model_name='webhooklistener',
            name='total_value',
            field=models.TextField(null=True, blank=True),
        ),
    ]
