# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_card_link_clicked'),
    ]

    operations = [
        migrations.CreateModel(
            name='WebhookListener',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('webhook_id', models.CharField(max_length=1024)),
                ('account_id', models.CharField(max_length=1024)),
                ('rest_of_the_data', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
