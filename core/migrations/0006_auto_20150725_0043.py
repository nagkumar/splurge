# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_webhooklistener'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webhooklistener',
            name='account_id',
            field=models.CharField(max_length=1024, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='webhooklistener',
            name='webhook_id',
            field=models.CharField(max_length=1024, null=True, blank=True),
        ),
    ]
