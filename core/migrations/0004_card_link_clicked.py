# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150724_0618'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='link_clicked',
            field=models.BooleanField(default=False),
        ),
    ]
