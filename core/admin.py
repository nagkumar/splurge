from django.contrib import admin

# Register your models here.
from .models import Card, AppUser, CardCategory, Relationship, WebhookListener


class RelationshipInline(admin.StackedInline):
    model = Relationship
    fk_name = 'from_person'


class AppUserAdmin(admin.ModelAdmin):
    search_fields = ("username", "first_name", "last_name", "email", )
    inlines = [RelationshipInline]


admin.site.register(AppUser, AppUserAdmin)
admin.site.register(Card)
admin.site.register(CardCategory)
admin.site.register(WebhookListener)
