import random
import string
from django.conf import settings
from django.template import Context
from django.template.loader import get_template
import requests

__author__ = 'nagkumar'


def random_string_generator(size=6, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


def send_confirm_email_message(user_email_id, unique_code):
    link = settings.BASE_URL + "/activate/" + unique_code + "/"
    html = get_template('email_confirm_address.html').render(Context({"link": link}))
    return requests.post(
        "https://api.mailgun.net/v3/mailgun.nagkumar.com/messages",
        auth=("api", settings.MAILGUN_API_KEY),
        data={"from": "Team Splurge <" + settings.SENDER_EMAIL_ID + ">",
              "to": user_email_id,
              "subject": settings.EMAIL_CONFIRM_SUBJECT,
              "text": "Please confirm your email address!",
              "html": html})


def send_new_card_notification(user_email_id, unique_code, sender_name):
    link = settings.BASE_URL + "/activate_card/" + unique_code + "/"
    html = get_template('email_new_card.html').render(Context({"link": link, "sender": sender_name}))
    return requests.post(
        "https://api.mailgun.net/v3/mailgun.nagkumar.com/messages",
        auth=("api", settings.MAILGUN_API_KEY),
        data={"from": "Team Splurge <" + settings.SENDER_EMAIL_ID + ">",
              "to": user_email_id,
              "subject": settings.EMAIL_NEW_CARD_NOTIFICATION_SUBJECT,
              "text": "You have received a new card",
              "html": html})