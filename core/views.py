import json
from django.utils import timezone
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext
import sys
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from validate_email import validate_email
from .helpers import send_confirm_email_message, send_new_card_notification
from .models import AppUser, CardCategory, Card, WebhookListener


def sign_up(request):
    if request.method == 'POST':
        password = request.POST.get("password")
        confirm_password = request.POST.get("confirm_password")
        if not password == confirm_password:
            return render_to_response("login.html", {
                'sign_up_error': True,
                'message': 'Passwords do not match'
            }, context_instance=RequestContext(request))
        username = request.POST.get("username")
        if AppUser.objects.filter(username=username).count() > 0:
            return render_to_response("login.html", {
                'sign_up_error': True,
                'message': 'Username is taken'
            }, context_instance=RequestContext(request))
        mobile_number = request.POST.get("phone")
        if AppUser.objects.filter(mobile_number=mobile_number).count() > 0:
            return render_to_response("login.html", {
                'sign_up_error': True,
                'message': 'Mobile Phone exists. Try logging in.'
            }, context_instance=RequestContext(request))
        email = request.POST.get("email")
        if AppUser.objects.filter(email=email).count() > 0:
            return render_to_response("login.html", {
                'sign_up_error': True,
                'message': 'Email exists. Try logging in.'
            }, context_instance=RequestContext(request))
        au = AppUser(username=username, email=email, mobile_number=mobile_number, user_type=settings.USER_TYPE_AH)
        au.save()
        au.set_password(password)
        au.save()
        send_confirm_email_message(au.email, au.unique_code)
        user = authenticate(username=username, password=password)
        login(request, user)
        return render_to_response("registration_success.html", context_instance=RequestContext(request))
    else:
        return render_to_response("login.html", context_instance=RequestContext(request))


@login_required(login_url='/login/')
def home(request):
    return render_to_response("home.html", context_instance=RequestContext(request))


def custom_log_out(request):
    logout(request)
    return render_to_response("login.html", {"login": True}, context_instance=RequestContext(request))


def custom_login(request):
    if not request.user.is_anonymous():
        return HttpResponseRedirect('/home/')
    if request.method == 'GET':
        return render_to_response("login.html", {'login': True}, context_instance=RequestContext(request))
    else:
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/home/')
        return render_to_response("login.html", {
            'login': True,
            'errors': True,
            'message': "Incorrect Username or password"
        }, context_instance=RequestContext(request))


def activate_user(request, code):
    try:
        user = AppUser.objects.get(unique_code=code)
        user.verified_email = True
        user.save()
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)
        return render_to_response("home.html", {
            "email_verified": True
        }, context_instance=RequestContext(request))
    except ObjectDoesNotExist:
        return render_to_response("invalid_code.html")


@login_required(login_url='/login')
def create_card(request):
    categories = CardCategory.objects.all()
    related_users = request.user.get_relationships()
    if request.method == 'POST':
        email = request.POST.get("email")
        if not validate_email(email):
            return render_to_response("create_card.html", {
                "errors": True,
                "message": "Invalid email",
                "categories": categories,
                "related_users": related_users
            }, context_instance=RequestContext(request))
        amount = int(request.POST.get("amount"))
        if request.user.balance < amount:
            return render_to_response("create_card.html", {
                "errors": True,
                "message": "Insufficient balance",
                "categories": categories,
                "related_users": related_users
            }, context_instance=RequestContext(request))
        # check if user exists
        user_entered_email = request.POST.get("email")
        receiver = None
        try:
            user_entered_email = int(user_entered_email)
            receiver = AppUser.objects.get(pk=user_entered_email)

        except ValueError:
            n = request.POST.get("fname")
            user_entered_name = []
            if " " in n:
                user_entered_name = n.split(" ")
            else:
                user_entered_name.append(n)
                user_entered_name.append("")
            new_user = AppUser(
                username=user_entered_email,
                email=user_entered_email,
                mobile_number=request.POST.get("phone"),
                first_name=user_entered_name[0],
                last_name=user_entered_name[1]
            )
            new_user.save()
            request.user.add_relationship(new_user)
            receiver = new_user

        # check if category exists
        user_category = request.POST.get("category")
        try:
            user_category = int(user_category)
            user_category = CardCategory.objects.get(pk=user_category)
        except ValueError:
            # new category
            new_category = CardCategory(name=user_category)
            new_category.save()
            user_category = new_category

        amount = request.POST.get("amount")
        amount = int(amount)
        request.user.balance -= int(amount)
        card = Card(
            category=user_category,
            creator=request.user,
            receiver=receiver,
            amount=amount
        )
        card.save()
        request.user.save()
        send_new_card_notification(user_entered_email, card.unique_code, request.user.username)
        return render_to_response("home.html", {"card_created": True}, context_instance=RequestContext(request))

    return render_to_response("create_card.html", {
        "categories": categories,
        "related_users": related_users
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def get_info(request, id):
    user = AppUser.objects.get(pk=int(id))
    return JsonResponse({
        'email': user.email,
        'mobile_number': user.mobile_number,
        'name': "%s %s" % (user.first_name, user.last_name)
    })


@login_required(login_url='/login/')
def list_cards(request):
    user = request.user
    cards = user.created_cards.all()
    return render_to_response("cards_list.html", {
        "user": user,
        "cards": cards
    }, context_instance=RequestContext(request))


def activate_card(request, code):
    print code
    try:
        card = Card.objects.get(unique_code=code)
        if card.link_clicked:
            raise ObjectDoesNotExist
        card.activated_on = timezone.now()
        card.link_clicked = True
        card.save()
        return render_to_response("card_activated.html", {
            "user": card.receiver,
            "card": card
        }, context_instance=RequestContext(request))
    except ObjectDoesNotExist:
        return render_to_response("invalid_code.html")
    except:
        print sys.exec_info()[0]


def welcome_page(request):
    return render_to_response("welcome.html")


@csrf_exempt
@api_view(['POST'])
def new_mail_received(request):
    print json.dumps(request.DATA)
    id = request.DATA.get("account_id")
    webhook_id = request.DATA.get("webhook_id")
    rest_of_the_data = request.DATA
    receipt = order_id = vendor = total_value = sender_email = sent_by = None
    person_info = request.DATA.get("message_data", None)
    if person_info:
        person_info = person_info.get("person_info", None)
        try:
            sender_email = person_info.get("message_data").get("addresses").get("from").get("email")
            sent_by = person_info.get("message_data").get("addresses").get("from").get("name")
        except AttributeError:
            pass
    receipt = request.DATA.get("parsed_receipts", None)
    if receipt:
        order_id = receipt[0].get("order_identifier", None)
        vendor = receipt[0].get("vendor", None)
        total_value = receipt[0].get("total", None)

    w = WebhookListener(
        webhook_id=webhook_id,
        account_id=id,
        person_info=person_info,
        order_id=order_id,
        vendor=vendor,
        receipt=receipt,
        total_value=total_value,
        sent_by=sent_by,
        sender_email=sender_email,
        rest_of_the_data=rest_of_the_data
    )
    w.save()
    return JsonResponse({})