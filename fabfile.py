from fabric.colors import red, green, cyan, magenta
from fabric.context_managers import cd, shell_env
from fabric.operations import run, sudo, local
from fabric.state import env

env.hosts = ["getsplurge.com"]
env.user = 'splurge'
dir_in_server = '/home/splurge/splurge'


def deploy(commit=False):
    if commit:
        print(cyan("Enter your git commit message"))
        msg = raw_input()
        local('git add .')
        local('git commit -am "%s"' % msg)
        print(green("Listing Branches"))
        local('git branch -a')
        print(cyan("Enter a branch name to push:"))
        branch = raw_input()
        local('git push origin %s' % branch)
        print(green("Deployment complete"))
    with cd(dir_in_server):
        print(magenta("Inside server"))
        run("git reset --hard || true")
        run("git pull origin master")
        run("source /home/splurge/splurge_env/bin/activate && pip install -r requirements.txt")
        with shell_env(DJANGO_SETTINGS_MODULE='splurge.settings.production'):
            run("source /home/splurge/splurge_env/bin/activate && ./manage.py collectstatic --noinput")
            run("source /home/splurge/splurge_env/bin/activate && ./manage.py migrate --no-initial-data")
        sudo("service apache2 restart")
    print(green("Deployment complete"))
